# Installation
## Linux
```sh:
git clone https://gitlab.com/bezy1/org-planner.git
cd ./org-planner
python main.py
```
### putting the program in one command
add this line to your .zshrc or .bashrc 
```sh:
orgpl="python /path/to/main.py"
```
# Commands
Note That the default catogeries are <P1,P2,P3,P4> and can be changed from the config.py file
| Commands|    Argument 1   | Argument 2 | Argument 3 |
|   ---   |      ---        |    ---     |    ---     | 
|  add    | <todo,block>    | catogery | TODO or BLOCK |
|  remove | <todo,block>    |    ID    |      NA    |
|  print  |      NA         |     NA     |      NA    |
## Examples 
```
orgpl add todo P1 Work on org planner 
orgpl add block 6:00-8:00 Org-Planner 
orgpl add todo P2 Fix Errors in org planner
orgpl print
orgpl remove todo 1 
```
