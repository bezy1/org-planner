import InputOutput as io
import os.path as op
import sys
from config import *
# init
if(date_naming):
    filename = io.GetName()
else:
    filename = name
if(not(op.isfile(filename))):
    open(filename,"x")
textlist = sys.argv
textlist = textlist[1:]
todos,blocks = io.Load(filename)
textlist,todos,blocks = io.CheckInput(filename,textlist,todos,blocks)
io.MakeFile(filename,io.Combine(todos,blocks))
