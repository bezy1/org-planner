from time import gmtime , strftime
from config import * 
def Combine(todos,blocks):
    text = ""
    text += "* TODOS\n"
    insert = 0
    for i in Sections: 
        insert = 0
        for j in todos:
            if(i in j):
                if(not(insert)):
                    text = text + "** " + i + '\n' 
                    insert = 1
                if(insert):
                    text += j[:abs(len(j) - len(i))] 
                
    text += "* TIME BLOCKS\n"
    for x in blocks:
        text += x
    return text


def GetName():
    date = strftime("%d%m%Y",gmtime())
    day = [int(date[0]) , int(date[1])]
    month =[ int(date[2]) , int(date[3])]
    year = [int(date[4]) , int(date[5]) , int(date[6]), int(date[7])]
    dim = [-1,31,28,31,30,31,30,31,31,30,31,30,31]
    if(not(month[0])):
       if(dim[month[1]] < int(f'{day[0]}{day[1]}')+1):
            month = str(int(f'{month[0]}{month[1]}')+1)
            day = '1'
            l = year
            year = ""
            for i in l:
                year += str(i)

       else:
            month = f'{month[1]}'
            day = str(int(f'{day[0]}{day[1]}') + 1)
            l = year
            year = ""
            for i in l: 
                year += str(i)
    else:
        if(dim[int(f'{month[0]}{month[1]}')] == int(f'{day[0]}{day[1]}')):  
            if(12 == int(f'{month[0]}{month[1]}')):
                month = '1'
                day = '1'
                l = year
                year = ""
                for i in l: 
                    year += str(i)
                year = str(int(year) + 1)
            else:
                month = str(int(f'{month[0]}{month[1]}') + 1)
                day = '1'
                l = year
                year = ""
                for i in l: 
                    year += str(i)
        else:
            month = f'{month[0]}{month[1]}' 
            day = str(int(f'{day[0]}{day[1]}') + 1)
            l = year
            year = ""
            for i in l: 
                year += str(i)
    return f'{day}-{month}-{year}.org'
def MakeFile(filename,text):
    p = open(filename,"w")
    p.write(text)
def UpdateFile(filename,todos,blocks):
    MakeFile(filename,Combine(todos,blocks))
def Load(filename):
    p = open(filename,"r")
    lines = p.readlines()
    todos,blocks = [],[]
    state = -1
    s = ""
    for i in lines:
        if(i == "* TODOS\n"):
            state = 1
        elif(i[0] == '*' and i[1] == '*'):
            for j in Sections:
                if(i == f'** {j}\n'): 
                    s = j
                    break
        elif(i == "* TIME BLOCKS\n"):
            state = 0
        elif(state == 1):
            todos.append(i + s) 
        elif(not(state)): 
            blocks.append(i)
    return todos,blocks
def CheckInput(filename,textlist,todos,blocks):
    if(textlist[0] == 'add'):
        x = ""
        if(textlist[1] == 'todo'):
            x = "- []"
            s = ""
            if(textlist[2] in Sections):
                s = textlist[2]
            for i in textlist[3:]:
                x += " " + i 
            x += '\n'
            x += s
            todos.append(x)
            return textlist , todos, blocks
        elif(textlist[1] == 'block'):
            x = "-"
            for i in textlist[2:]:
                x += " " + i
            x += "\n"
            blocks.append(x)
            return textlist , todos, blocks
        else:
            print("Usage : add <todo,block> something")
    elif(textlist[0] == 'remove'):
        if(textlist[1] == 'todo'):
            del todos[int(textlist[2])]
        elif(textlist[1] == 'block'):
            del blocks[int(textlist[2])]
        return textlist , todos, blocks
    elif(textlist[0] == 'print'):
            print("TODOS")
            for i in range(len(todos)):
                print(i,"|",todos[i])
            print("BLOCKS")
            for i in range(len(blocks)):
                print(i,"|",blocks[i])
            return textlist,todos,blocks
    elif(textlist[0] == 'update'):
        UpdateFile(filename,todos,blocks)
        return textlist,todos,blocks
    else:
        print("Usage <add,remove> <todo,block> <id>")
        print("type help for more : -h")
